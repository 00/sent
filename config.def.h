/* See LICENSE file for copyright and license details. */

static char *fontfallbacks[] = {
	"dejavu sans",
	"roboto",
	"ubuntu",
};
#define NUMFONTSCALES 42
#define FONTSZ(x) ((int)(10.0 * powf(1.1288, (x)))) /* x in [0, NUMFONTSCALES-1] */

static const char *colors[] = {
	"#FFFFFF", /* foreground color */
	"#000000", /* background color */
};

static const char *inverted_colors[] = {
	"#000000", /* foreground color */
	"#FFFFFF", /* background color */
};

static const float linespacing = 1.4;

/* how much screen estate is to be used at max for the content */
static const float usablewidth = 0.75;
static const float usableheight = 0.75;

static Mousekey mshortcuts[] = {
	/* button         function        argument */
	{ Button1,        advance,        {.i = +1} },
	{ Button3,        advance,        {.i = -1} },
};

static Shortcut shortcuts[] = {
	/* keysym         function        argument */
	{ XK_Escape,      quit,           {0} },
	{ XK_Right,       advance,        {.i = +1} },
	{ XK_Left,        advance,        {.i = -1} },
	{ XK_k,           advance,        {.i = +1} },
	{ XK_j,           advance,        {.i = -1} },
	{ XK_r,           reload,         {0} },
	{ XK_i,           togglescm,      {0} },
};

static Filter filters[] = {
	{ "\\.ff$", "cat" },
	{ "\\.ff.bz2$", "bunzip2" },
	{ "\\.[a-z0-9]+$", "2ff" },
};
